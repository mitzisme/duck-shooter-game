# Duck Shooter game

Duck Shooter is a FPS game where players can shoot ducks that are randomly generated in the sky for points. Players can also pick up items and make their own personal high-score. The game has a countdown for each session which is one minute.