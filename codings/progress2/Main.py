# Basic OBJ file viewer. needs objloader from:
#  http://www.pygame.org/wiki/OBJFileLoader
# LMB + move: rotate
# RMB + move: pan
# Scroll wheel: zoom in/out
import array
import pyautogui
import pygame
import numpy
import random
import sys
from math import sin, cos, radians
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from pygame.constants import *
from pygame.locals import *

# IMPORT OBJECT LOADER
from objloader import *


def drawText(text):
    font = pygame.font.SysFont("consolas", 42)
    textSurface = font.render(text, True, (255, 255, 255, 255), (0, 0, 0, 255))
    textData = pygame.image.tostring(textSurface, "RGBA", True)
    glRasterPos3d(0, 20, 0)
    glDrawPixels(textSurface.get_width(), textSurface.get_height(),
                 GL_RGBA, GL_UNSIGNED_BYTE, textData)


pygame.init()
viewport = (800, 600)
width, height = viewport
hx = viewport[0]/2
hy = viewport[1]/2
srf = pygame.display.set_mode(viewport, OPENGL | DOUBLEBUF)

glLightfv(GL_LIGHT0, GL_POSITION,  (-40, 200, 100, 0.0))
glLightfv(GL_LIGHT0, GL_AMBIENT, (0.2, 0.2, 0.2, 1.0))
glLightfv(GL_LIGHT0, GL_DIFFUSE, (0.5, 0.5, 0.5, 1.0))
glEnable(GL_LIGHT0)
glEnable(GL_LIGHTING)
glEnable(GL_COLOR_MATERIAL)
glEnable(GL_DEPTH_TEST)
glShadeModel(GL_SMOOTH)           # most obj files expect to be smooth-shaded

glMatrixMode(GL_PROJECTION)
glLoadIdentity()
gluPerspective(90.0, width/float(height), 1, 100.0)
glMatrixMode(GL_MODELVIEW)
glLoadIdentity()

# VARIABLES
rx, ry = (5, 0)
tx, ty = (10, 0)
zpos = 10
rotate = move = False
w = False
a = False
s = False
d = False
totalDuck = 1
list_duck = []
index = random.randint(0, totalDuck - 1)
location_x, location_y, location_z = (0.0, 8.0, 0.0)
lookat_x, lookat_y, lookat_z = (0.0, 8.0, 5.0)
lookup_x, lookup_y, lookup_z = (0.0, 1.0, 0.0)
angleX = 0.0
angleY = 0.0

# LOAD OBJECT AFTER PYGAME INIT
clock = pygame.time.Clock()
terrain = Terrain("terrain.obj", swapyz=True)
world = World("world.obj", swapyz=True)
rifle= Rifle("rifle.obj",swapyz=True)

for i in range(0, totalDuck):
    duck = Duck("duck.obj", swapyz=True)
    list_duck.append(duck)

while True:
    clock.tick(50)

    for e in pygame.event.get():
        if e.type == MOUSEMOTION:
            i, j = e.rel
            if i > 0:
                angleX += 0.06
                lookat_x = float(sin(angleX)) + float(location_x)
            else:
                angleX -= 0.06
                lookat_x = float(sin(angleX)) + float(location_x)

            if j > 0 and lookat_y > location_y - 0.9:
                angleY += 0.06
                lookat_y = float(-cos(angleY)) + float(location_y)
            elif j < 0 and lookat_y < location_y + 0.9:
                angleY -= 0.06
                lookat_y = float(-cos(angleY)) + float(location_y)

            lookat_z = float(-cos(angleX)) + float(location_z)

        if e.type == pygame.KEYDOWN:
            if e.key == pygame.K_w:
                w = True
            if e.key == pygame.K_a:
                a = True
            if e.key == pygame.K_s:
                s = True
            if e.key == pygame.K_d:
                d = True
        if e.type == pygame.KEYUP:
            if e.key == pygame.K_w:
                w = False
            elif e.key == pygame.K_a:
                a = False
            elif e.key == pygame.K_s:
                s = False
            elif e.key == pygame.K_d:
                d = False

        if e.type == MOUSEBUTTONDOWN:
            if e.button == 4:
                zpos = max(1, zpos-1)
            elif e.button == 5:
                zpos += 1
            elif e.button == 1:
                rotate = True
            elif e.button == 3:
                move = True
        elif e.type == MOUSEBUTTONUP:
            if e.button == 1:
                rotate = False
            elif e.button == 3:
                move = False
        # if rotate:
        #    rx += i
        #    ry += j
        # if move:
        #    tx += i
        #    ty -= j

    if w == True:
        location_x += float(sin(angleX) * 1)
        lookat_x += float(sin(angleX) * 1)
        location_z -= float(cos(angleX) * 1)
        lookat_z -= float(cos(angleX) * 1)
    if a == True:
        location_x -= float(cos(angleX) * 1)
        lookat_x -= float(cos(angleX) * 1)
        location_z -= float(sin(angleX) * 1)
        lookat_z -= float(sin(angleX) * 1)
    if s == True:
        location_x -= float(sin(angleX) * 1)
        lookat_x -= float(sin(angleX) * 1)
        location_z += float(cos(angleX) * 1)
        lookat_z += float(cos(angleX) * 1)
    if d == True:
        location_x += float(cos(angleX) * 1)
        lookat_x += float(cos(angleX) * 1)
        location_z += float(sin(angleX) * 1)
        lookat_z += float(sin(angleX) * 1)

    list_duck[index].x += list_duck[index].dx
    if list_duck[index].x > 60:
        list_duck[index].dx *= -1
        index = random.randint(0, totalDuck - 1)
    elif list_duck[index].x < -60:
        list_duck[index].dx *= -1
        index = random.randint(0, totalDuck - 1)

    # glTranslate(tx/20., ty/20., - zpos)
    # glRotate(ry, 1, 0, 0)
    # glRotate(rx, 0, 1, 0)

    glLoadIdentity()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    # CAMERA
    gluLookAt(location_x, location_y, location_z, lookat_x,
              lookat_y, lookat_z, lookup_x, lookup_y,  lookup_z)
            
    # RIFLE
    glPushMatrix()
    glTranslate(lookat_x,lookat_y,lookat_z)
    glCallList(rifle.gl_list)
    glPopMatrix()

    # DUCK
    for i in range(0, len(list_duck)):
        glPushMatrix()
        glTranslate(list_duck[i].x, list_duck[i].y, list_duck[i].z)
        glCallList(list_duck[i].gl_list)
        glPopMatrix()

    drawText("#NGULANGGRAFKOM")
    glCallList(terrain.gl_list)

    pygame.display.flip()
